//
//  Logger.swift
//
//  Created by Jan Mazurczak on 24/09/2019.
//  Copyright © 2019 Jan Mazurczak. All rights reserved.
//

import Foundation

public class Logger {
    
    public static let shared = Logger()
    #if DEBUG
    public var isActive = true
    #else
    public var isActive = false
    #endif
    private init() {}
    
    public func log(_ entry: String) {
        guard isActive else { return }
        NSLog(entry)
        TextFileLogger.current.log(entry)
    }
    
}
