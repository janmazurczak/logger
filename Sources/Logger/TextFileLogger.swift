//
//  TextFileLogger.swift
//  CommonElements
//
//  Created by Jan Mazurczak on 07/04/2017.
//  Copyright © 2017 Jan Mazurczak. All rights reserved.
//
//  The MIT License (MIT)
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import Foundation


open class TextFileLogger {
    
    public private(set) static var current = TextFileLogger()
    
    private let queue: OperationQueue = {
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    public private(set) var text: String
    private let url: URL?
    
    private init() {
        let now = Date()
        let timestamp = DateFormatter.logs.string(from: now)
        let filename = timestamp + " " + UUID().uuidString
        text = filename + "\n"
        url = URL.logs.appendingPathComponent(filename + ".txt")
    }
    
    public func log(_ log: String) {
        queue.addOperation {
            guard self.text.count < 100000 else {
                TextFileLogger.current = TextFileLogger()
                TextFileLogger.current.log(log)
                return
            }
            let now = Date()
            let timestamp = DateFormatter.logs.string(from: now)
            self.text.append(timestamp + ": " + log + "\n")
            self.save()
        }
    }
    
    private func save() {
        guard let url = url else { return }
        try? text.write(to: url, atomically: true, encoding: .utf8)
    }
}


private extension URL {
    
    static let logs: URL = {
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("Logs", isDirectory: true)
        try? FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
        return url
    }()
    
}


private extension DateFormatter {
    
    static let logs: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
}




